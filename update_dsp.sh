cp .gitignore .gitignore_
mv .git .git_
mv git_disabled .git

## update 
git stash
git checkout master
git pull origin master
//self-update is optional, you might get an error without the latest copy of composer
composer self-update
composer install --no-dev
php artisan migrate --seed
php artisan cache:clear

sudo chown -R www-data:www-data storage/ bootstrap/cache/
sudo chmod -R 2775 storage/ bootstrap/cache/


mv .git git_disabled 
mv .git_ .git
rm .gitignore 
mv .gitignore_ .gitignore

git add --all 
git commit -m update
git push

